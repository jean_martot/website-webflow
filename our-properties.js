document.addEventListener("alpine:init", () => {
    Alpine.data('properties', () => ({
        filter: {
            type: "",
            where: "",
            price: "",
            bedroom: ""
        },
        spinner: false,
        properties: [],
        propertiesChunked: [],
        whereOptions: [],
        typeOptions: [],
        bedroomOptions: [],
        priceOptions: [
          {'text': "0 € - 500 000 €", 'value': "price-cat-1"},
          {'text': "500.000 € - 1.000.000 €", 'value': "price-cat-2"},
          {'text': "1.000.000 € - 2.000.000 €", 'value': "price-cat-3"},
          {'text': "2.000.000 € - 3.000.000 €", 'value': "price-cat-4"},
          {'text': "3.000.000 € - 4.000.000 €", 'value': "price-cat-5"},
          {'text': "4.000.000 € et +", 'value': "price-cat-6"}
        ],
        page: 1,
        pageNumber: 0,
        request: {
            next: true,
            offset_param: "",
        },
        whereSelected: "*",
        async init() {
            try {
                console.log('Init properties');
                this.spinner = true;
                while (this.request.next) {
                    /* Get properties from CMS webflow */
                    var myHeaders = new Headers();
                    myHeaders.append("Authorization", "Bearer keyEY90Agz1Zs2l9h");
                    myHeaders.append("Cookie", "brw=brwyExQCyl51dN17C");

                    var requestOptions = {
                      method: 'GET',
                      headers: myHeaders,
                      redirect: 'follow'
                    };

                    let propertiesRequest = await fetch(`https://api.airtable.com/v0/appXmK7xdWKfNpfrx/Properties?${this.request.offset_param}pageSize=50&view=Ready%20to%20online&fields%5B%5D=Property%20Code&fields%5B%5D=Title&fields%5B%5D=Slug&fields%5B%5D=Status&fields%5B%5D=Bedroom%28s%29&fields%5B%5D=City&fields%5B%5D=State&fields%5B%5D=Type&fields%5B%5D=Price&fields%5B%5D=Images&fields%5B%5D=Short%20description%20%28%3C%20180%20chars%29&fields%5B%5D=Living%20Space%20square%20size%20%28m2%29&fields%5B%5D=Plot%20square%20size%20%28m2%29`, requestOptions);
                    let resultProperties = await propertiesRequest.json();

                    if (propertiesRequest.status !== 200) throw resultProperties;
                    if (resultProperties.hasOwnProperty('offset') === true) {
                        console.log(resultProperties.hasOwnProperty('offset'));
                        this.request.offset_param = `offset=${resultProperties.offset}&`;
                        this.request.next = true;
                    } else {
                        console.log(resultProperties.hasOwnProperty('offset'));
                        this.request.offset_param = null;
                        this.request.next = false;
                    }

                    console.log("propertiesRequest:", propertiesRequest);
                    console.log("resultProperties:", resultProperties);

                    /* Update data */
                    this.properties = [...this.properties, ...resultProperties.records];
                    this.propertiesChunked = this.chunk(this.properties, 10);
                }

                console.log("properties:", this.properties);
                console.log("properties Chunked:", this.propertiesChunked);

                //set Pagination
                this.pageNumber = this.propertiesChunked.length;

                /* Parse properties */
                console.log("foreach");
                this.properties.forEach((property) => {
                    //If typeOptions already exist don't push it
                    if(this.typeOptions.filter(typeOption => property.fields.Type.trim().toLowerCase() === typeOption.text).length === 0) {
                        this.typeOptions.push(
                            {'text': property.fields.Type.trim().toLowerCase(), 'value': property.fields.Type.trim().toLowerCase().replace(/\s/g, '-')}
                        );

                    }

                    //If whereOptions already exist don't push it
                    if(this.whereOptions.filter(whereOption => property.fields.City.trim().toLowerCase() === whereOption.text).length === 0) {
                        this.whereOptions.push(
                            {'text': property.fields.City.trim().toLowerCase(), 'value': property.fields.City.trim().toLowerCase().replace(/\s/g, '-')}
                        );

                    }

                    //If bedroomOptions already exist don't push it
                    if(this.bedroomOptions.filter(bedroomOption => property.fields['Bedroom(s)'].toString().trim().toLowerCase() === bedroomOption.text).length === 0) {
                        this.bedroomOptions.push(
                            {'text': property.fields['Bedroom(s)'].toString().trim().toLowerCase(), 'value': property.fields['Bedroom(s)'].toString().trim().toLowerCase().replace(/\s/g, '-')}
                        );

                    }
                })

                console.log("typeOptions", this.typeOptions);
                console.log("whereOptions", this.whereOptions);
                console.log("bedroomOptions", this.bedroomOptions);

                //Populate select with URL parameter
                if (this.getUrlParameter('where') != false && this.getUrlParameter('type') != false && this.getUrlParameter('price') != false) {
                  this.whereSelected = this.getUrlParameter('where');
                  this.$refs.typeSelect.value = this.getUrlParameter('type');
                  this.$refs.priceSelect.value = this.getUrlParameter('price');

                  console.log(this.whereSelected);
                  console.log(this.$refs.typeSelect.value);
                  console.log(this.$refs.priceSelect.value);

                  this.filter();
                }
                this.$nextTick(() => {
                  this.loadSlider();
                  Weglot.initialize({
                      api_key: 'wg_09a4e82f607dfeaa85edfea6971cf31e6',
                      dynamic: ".propertie-collection-list-2",
                  });
                })

                //Init all carouselle
                this.spinner = false;
            } catch (e) {
                console.log('error', e);
            }
        },
        chunk (array, chunkSize) {
          var R = [];
          for (var i = 0; i < array.length; i += chunkSize)
            R.push(array.slice(i, i + chunkSize));
          return R;
        },
        formatePrice(price) {
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(price);
        },
        prevPage() {
          if (this.page != 1) {
            this.page -= 1;
          } 
        },
        nextPage() {
          if (this.page != this.pageNumber) {
            this.page += 1;
          } 
        },
        loadSlider() {
            var elms = document.getElementsByClassName( 'splide' );

            for ( var i = 0; i < elms.length; i++ ) {
              new Splide( elms[ i ], {
                        cover      : true,
                        wheel       : true,
                          releaseWheel: true,
                  } ).mount();
            }
        },
        filter() {
            console.log(this.whereSelected);
            console.log(this.$refs.typeSelect.value);
            console.log(this.$refs.priceSelect.value);

            //1 - Filter all properties (propertyObj)
            const propertyFiltred = this.properties.filter((property) => {
              var priceParsed = parseInt(property.fields.Price);

              if (priceParsed > 0 && priceParsed <= 500000) {
                priceParsed = "price-cat-1";
              } else if (priceParsed > 500000 && priceParsed <= 1000000) {
                priceParsed = "price-cat-2";
              } else if (priceParsed > 1000000 && priceParsed <= 2000000) {
                priceParsed = "price-cat-3";
              } else if (priceParsed > 2000000 && priceParsed <= 3000000) {
                priceParsed = "price-cat-4";
              } else if (priceParsed > 3000000 && priceParsed <= 4000000) {
                priceParsed = "price-cat-5";
              } else { 
                priceParsed = "price-cat-6";
              }

              let typeParsed = "";

              if (property.fields.Type === "Apartment") {
                typeParsed = "type-cat-1"
              } else if (property.fields.Type === "Commercial") {
                typeParsed = "type-cat-2"
              } else if (property.fields.Type === "Contemporary Villa") {
                typeParsed = "type-cat-3"
              } else if (property.fields.Type === "Traditional Villa") {
                typeParsed = "type-cat-4"
              } else if (property.fields.Type === "Townhouse") {
                typeParsed = "type-cat-5"
              } else if (property.fields.Type === "Land") {
                typeParsed = "type-cat-6"
              } else {
                typeParsed = "type-cat-7"
              }

              if ((this.whereSelected === property.fields.City.trim().toLowerCase().replace(/\s/g, '-') || this.whereSelected === "*") && 
                  (this.$refs.typeSelect.value === typeParsed || this.$refs.typeSelect.value === "*") &&
                  (this.$refs.priceSelect.value === priceParsed || this.$refs.priceSelect.value === "*")) {
                return true;
              } else {
                return false;
              }
            });

            //2 - Chunk the properties filtered
            this.propertiesChunked = this.chunk(propertyFiltred, 10);
            //3 - Reset pagination
            this.page = 1;
            this.pageNumber = this.propertiesChunked.length;

            this.loadSlider();

            this.$nextTick(() => {
              this.loadSlider();
              Weglot.initialize({
                  api_key: 'wg_09a4e82f607dfeaa85edfea6971cf31e6',
                  dynamic: ".propertie-collection-list-2",
              });
            })

            console.log("- Filter launched");
            console.log("propertyObj: ", this.properties);
            console.log("propertyFiltred: ", propertyFiltred);
            console.log("propertyFiltredAndChunk: ", this.propertiesChunked);
            console.log("--------");
        },
        getUrlParameter(sParam) {
          var sPageURL = window.location.search.substring(1),
              sURLVariables = sPageURL.split('&'),
              sParameterName,
              i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
              }
          }
          return false;
        }
    }));
});