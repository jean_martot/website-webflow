<script>
    document.addEventListener("DOMContentLoaded", function() {
    /* --- Create option lists --- */
    var whereOptions = []
    var typeOptions = []

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    Array.prototype.forEach.call($("#filter-property-list").children('div'), child => {
      whereOptions.push(`${child.querySelector('#where-city-option').childNodes[0].nodeValue.trim().toLowerCase()}`);
    });

    whereOptions = whereOptions.filter(onlyUnique).map(option => ({'text': option, 'value': option.replace(/\s/g, '-')}));
        
    $('#dropdown-property-state').empty();
    $('#dropdown-property-state').append('<option value="' + '*' + '">' + 'Anywhere' + '</option>');
    for (let i = 0; i < whereOptions.length; i++) {
      $('#dropdown-property-state').append('<option value="' + whereOptions[i].value + '">' + whereOptions[i].text + '</option>');
    }
    
    /* --- Filter search button redirection ---*/
    $( "#filter-search-button" ).click(function( event ) {
      const where = $("#dropdown-property-state option:selected").val();
      const type = $("#dropdown-property-type option:selected").val();
      const price = $("#dropdown-property-price option:selected").val();

      window.location = `/our-properties?where=${where}&type=${type}&price=${price}`;
    });
  });
</script>